# Distributed Systems CWK2

## Set up

To set up the environments for the services, run the following command \
```sh setup.sh```

## Running the servers

To run the servers for the two REST APIs and the client, run the following command \
```sh run.sh```

Note: this will run the services on the following hosts and ports:

- service 1: localhost:5000
- service 2: localhost:5001
- client web server: localhost:5003

## Testing

Testing the two REST API services running locally, and the external service, can be done by running the following command \
```sh runTests.sh```

