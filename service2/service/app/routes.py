from app import app
from flask import request
from flask_restful import Resource, Api
from enum import Enum

# Needed to resolve the import of the the metrics.py file
import sys
sys.path.append('app/metrics')
from metrics import Metric, MetricType

api = Api(app)

def checkEssentialPostData(dictToCheck, *essentialAttributes):
  return all(attribute in dictToCheck for attribute in essentialAttributes)


class StringLikeness(Resource):

  def post(self):
    data = request.get_json()
    if not checkEssentialPostData(data, "metricType", "string1", "string2"):
      return {"error": "JSON does not include the required data to calculate a string likeness measurement"}, 400
      
    metricType = data["metricType"]
    string1 = data["string1"]
    string2 = data["string2"]

    if metricType.upper() == "LEVENSHTEIN":
      metric = Metric.create(MetricType.LEVENSHTEIN, string1, string2)
      likenessValue = metric.calculate()
      return {"likenessValue" : likenessValue}
    if metricType.upper() == "SEQUENCEMATCH":
      metric = Metric.create(MetricType.SEQUENCE_MATCH, string1, string2)
      likenessValue = metric.calculate()
      return {"likenessValue" : likenessValue}
    else:
      return {"error": "metricType: '" + metricType + "' is not a valid metricType - must be one of ('levenshtein'. 'sequencematch')"}, 400

api.add_resource(StringLikeness, "/StringLikeness")
