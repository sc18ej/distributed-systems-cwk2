from enum import Enum
from abc import ABC, abstractmethod
from difflib import SequenceMatcher

class MetricType(Enum):
  LEVENSHTEIN = 0
  SEQUENCE_MATCH = 1

class Metric(ABC):

  @staticmethod
  def create(stringLikenessMetric, string1, string2):
    if stringLikenessMetric == MetricType.LEVENSHTEIN:
      return LevenshteinMetric(string1, string2)
    if stringLikenessMetric == MetricType.SEQUENCE_MATCH:
      return SequenceMatchMetric(string1, string2)

  @abstractmethod
  def calculate(self):
    pass

class SequenceMatchMetric(Metric):

  def __init__(self, string1, string2):
    self.string1 = string1
    self.string2 = string2

  def calculate(self):
    sequenceMatch = SequenceMatcher(None, self.string1, self.string2)
    return sequenceMatch.ratio()

class LevenshteinMetric(Metric):

  @staticmethod
  def calculateLevenshteinDynamic(string1, string2):
    m = len(string1)
    n = len(string2)

    matrix = [[0 for i in range(n + 1)] for j in range(m + 1)]

    for i in range(m + 1):
      matrix[i][0] = i

    for i in range(n + 1):
      matrix[0][i] = i

    for i in range(1, m + 1):
      for j in range(1, n + 1):
        if (string1[i - 1] == string2[j - 1]):
          substitution = 0
        else:
          substitution = 1
        
        matrix[i][j] = min(matrix[i - 1][j] + 1,               # deletion
                          matrix[i][j - 1] + 1,                # insertion
                          matrix[i - 1][j - 1] + substitution) # substitution

    return matrix[m][n]

  def __init__(self, string1, string2):
    self.string1 = string1
    self.string2 = string2

  def calculate(self):
    m = len(self.string1)
    n = len(self.string2)

    maxStringSize = max(m, n)

    levenshteinDistance = self.calculateLevenshteinDynamic(self.string1, self.string2)

    return (maxStringSize - levenshteinDistance) / maxStringSize