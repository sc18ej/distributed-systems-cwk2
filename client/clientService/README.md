# Client

## Initialising the venv

### Setup and starting the venv
- In the directory client/ run the command `python3 -m venv venv`
- Then run the command `. venv/bin/activate` to enter the virtual environment

### Installing the dependencies in the venv
- Once you have entered the venv, navigate to the directory client/clientService/
- Then run the command `pip install -r requirements.txt` to install the modules listed in the *requirements.txt* file

