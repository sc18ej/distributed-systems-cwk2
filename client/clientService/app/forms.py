from flask_wtf import FlaskForm
from wtforms import HiddenField, SubmitField

class CompleteQuoteForm(FlaskForm):
    quoteText = HiddenField("quoteText", default="NO_QUOTE_SET")
    authorText = HiddenField("authorText", default="NO_AUTHOR_SET")
    submit = SubmitField("Pass the quote to the AI")

class CompareQuoteForm(FlaskForm):
    quoteText = HiddenField("quoteText", default="NO_QUOTE_SET")
    completedQuoteText = HiddenField("completedQuoteText", default="NO_COMPLETED_QUOTE_SET")
    authorText = HiddenField("authorText", default="NO_AUTHOR_SET")
    promptText = HiddenField("promptText", default="NO_PROMPT_SET")
    submit = SubmitField("Compare quote and AI response")