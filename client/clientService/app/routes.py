from app import app, forms
from flask import render_template, url_for, redirect, request, abort
import requests

SERVICE1_DOMAIN = "http://localhost:5000/"
SERVICE2_DOMAIN = "http://localhost:5001/"
GPT3_DOMAIN     = "https://api.openai.com/v1/engines/davinci/completions"

GPT3_API_KEY    = "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"

'''
Route for homepage
'''
@app.route("/")
def index():
  return render_template("index.html")

'''
Route for random quote from database

:GET: Method for getting random quote page
:POST: Method which executes on hidden quote form submit
'''
@app.route("/randomQuote", methods=["GET"])
def randomQuote():

  form = forms.CompleteQuoteForm()

  # Utilises the first service to get a random quote
  randomQuoteResponse = requests.get(SERVICE1_DOMAIN + "Quotes/random")
  responseJSON = randomQuoteResponse.json()

  # Create form to request completion of the quote
  form = forms.CompleteQuoteForm()

  quote = responseJSON["quoteText"]
  author = responseJSON["author"]

  return render_template("randomQuote.html", quote = quote, author = author, form = form)

'''
Route for completing quotes

:POST: Method which executes on hidden quote form submit
'''
@app.route("/completedQuote", methods=["POST"])
def completedQuote():

  form = forms.CompareQuoteForm()

  quoteText = request.form["quoteText"]
  authorText = request.form["authorText"]

  # Assembling header and payload for GPT-3 request
  prompt = " ".join(quoteText.split(" ")[:(quoteText.count(" ") + 1)//2])
  payload = {"prompt": prompt, "max_tokens": (quoteText.count(" ") + 1) + 5}
  headers = {"content-type": "application/json", "Authorization": "Bearer " + GPT3_API_KEY}
  gpt3Response = requests.post(GPT3_DOMAIN, headers=headers, json=payload)

  completedQuote = ""

  # Checking GPT-3 response
  if gpt3Response.status_code != 200:
    completedQuote = gpt3Response.json()['error']
  else:
    gpt3ResponseJSON = gpt3Response.json()
    completedQuote = prompt + gpt3ResponseJSON["choices"][0]["text"] + "..."

  return render_template("completedQuote.html", quote = quoteText, author = authorText, prompt = prompt, completedQuote = completedQuote, form = form)

@app.route("/compareQuote", methods = ["POST"])
def compareQuote():
  quoteText = request.form["quoteText"]
  completedQuoteText = request.form["completedQuoteText"]
  authorText = request.form["authorText"]
  promptText = request.form["promptText"]

  completedQuoteTextGeneratedPart = completedQuoteText[len(promptText):len(quoteText)]
  quoteTextActualPart = quoteText[len(promptText):]

  payloadLevenshtein = {
    "metricType": "LEVENSHTEIN",
    "string1": quoteTextActualPart,
    "string2": completedQuoteTextGeneratedPart
  }

  levenshteinResponse = requests.post(SERVICE2_DOMAIN + "StringLikeness", json=payloadLevenshtein)
  levenshteinSimilarity = round(float(levenshteinResponse.json()["likenessValue"]) * 100)

  return render_template("compareQuote.html", quote = quoteText, author = authorText, prompt = promptText, completedQuote = completedQuoteText, levenshteinSimilarity = levenshteinSimilarity)

'''
Route for 404 error
'''
@app.errorhandler(404)
def pageNotFound(e):
  return render_template("404.html"), 404