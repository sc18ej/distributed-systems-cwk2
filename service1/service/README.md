# Service 1

## Initialising the venv

### Setup and starting the venv
- In the directory service1/ run the command `python3 -m venv venv`
- Then run the command `. venv/bin/activate` to enter the virtual environment

### Installing the dependencies in the venv
- Once you have entered the venv, navigate to the directory service1/service/
- Then run the command `pip install -r requirements.txt` to install the modules listed in the *requirements.txt* file

## How to initialise the database

### Creating a blank database
- Run the command `flask db init` in the directory service1\service
- After this command, run `flask db migrate`
- Then `flask db upgrade`

### Populating the blank database
- Run the script db_populate.py using the command `python3 db_populate.py`

