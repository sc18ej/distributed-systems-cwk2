from app import app, models, db
from uuid import uuid1
import sys, csv

print("POPULATING DATABASE, THIS MAY TAKE ~30 SECONDS...")

firstLine = True

# Opening the csv file full of quotes and iterating through each line
with open("quotes.csv", newline="\n") as csvfile:
    quotesFile = csv.reader(csvfile, quotechar='"', delimiter=',', quoting=csv.QUOTE_ALL, skipinitialspace=True)

    for record in quotesFile:
        if firstLine:
            firstLine = False
        else:

            # Cleaning data slightly by removing commas from the end of some Author's names
            record[2] = record[2].replace(",", "")

            # Aborting if record causes an error when fitting to the db model
            try:
                newQuote = models.Quote(quoteID=uuid1().hex, quoteText=record[1], author=record[2], tags=record[3], likes=int(record[4]))
            except:
                print("ERROR OCCURRED WITH RECORD BELOW, ABORTING...")
                print(record)
                sys.exit(1)

            # Commiting changes to db
            db.session.add(newQuote)
            db.session.commit()

print("SUCCESSFULLY POPULATED DATABASE")
