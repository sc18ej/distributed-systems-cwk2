from app import app, models, db
from flask_restful import Resource, Api
from flask_cors import CORS
from random import choice

api = Api(app)
CORS(app)

# REST Operations for Quotes with specified ID
class QuoteResource(Resource):

    def get(self, quoteID):

        # If the specified ID is the string 'random', return a random quote
        if quoteID == "random":
            quote = choice(models.Quote.query.all())
        else:
            quote = models.Quote.query.filter_by(quoteID=quoteID).first()
            if not quote:
                return {"error": "No Quote has the specified ID"}, 404

        return quote.toJSON(), 200

# REST Operations for all Quotes
class QuotesResource(Resource):

    def get(self):
        return [quote.toJSON() for quote in models.Quote.query.all()], 200

# Adding routes for resources

api.add_resource(QuoteResource, "/Quotes/<string:quoteID>")
api.add_resource(QuotesResource, "/Quotes")
