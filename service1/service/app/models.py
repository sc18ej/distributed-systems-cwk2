from app import db

# Database model for a Quote object
class Quote(db.Model):

    quoteID = db.Column(db.String(36), primary_key=True)
    quoteText = db.Column(db.Text, nullable=False)
    author = db.Column(db.Text, nullable=False)
    tags = db.Column(db.Text, nullable=False)
    likes = db.Column(db.Integer, nullable=False)

    # Defining a toJSON method for this model
    def toJSON(self):
        return {"quoteID": self.quoteID, "quoteText": self.quoteText, "author": self.author, "tags": self.tags, "likes": str(self.likes)}
