import requests, time

GPT3_DOMAIN = "https://api.openai.com/v1/engines/davinci/completions"
GPT3_API_KEY = "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"
fileName = "remoteService_results.txt"

string1 = "test String 1"

def timeStamp():
    return round(time.time() * 1000)

with open(fileName, "w") as f:

    for i in range(5):

        timeStart = timeStamp()
        payload = {"prompt": string1, "max_tokens": (string1.count(" ") + 1) + 5}
        headers = {"content-type": "application/json", "Authorization": "Bearer " + GPT3_API_KEY}
        response = requests.post(GPT3_DOMAIN, headers=headers, json=payload)
        timeEnd = timeStamp()

        if response.status_code == 200:
            f.write("TEST " + str(i+1) + " --> STATUS CODE 200 - RESPONSE OK\nresponse time = " + str(timeEnd - timeStart) + "\n\n")
        else:
            f.write("TEST " + str(i+1) + " --> STATUS CODE " + str(response.status_code) + " - REQUEST FAILED\nresponse time = " + str(timeEnd - timeStart) + "\n\n")
            
    f.close()

print("Printed results to file '" + str(fileName) + "'")