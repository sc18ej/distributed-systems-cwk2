import requests, time

SERVICE2_DOMAIN = "http://localhost:5001/"
fileName = "service2_results.txt"

string1 = "test String 1"
string2 = "test String 2"

def timeStamp():
    return round(time.time() * 1000)

with open(fileName, "w") as f:

    for i in range(5):

        timeStart = timeStamp()
        payload = {
            "metricType": "LEVENSHTEIN",
            "string1": string1,
            "string2": string2
        }

        response = requests.post(SERVICE2_DOMAIN + "StringLikeness", json=payload)
        timeEnd = timeStamp()

        if response.status_code == 200:
            f.write("TEST " + str(i+1) + " --> STATUS CODE 200 - RESPONSE OK\nresponse time = " + str(timeEnd - timeStart) + "\n\n")
        else:
            f.write("TEST " + str(i+1) + " --> STATUS CODE " + str(response.status_code) + " - REQUEST FAILED\nresponse time = " + str(timeEnd - timeStart) + "\n\n")
            
    f.close()

print("Printed results to file '" + str(fileName) + "'")