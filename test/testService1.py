import requests, time

SERVICE1_DOMAIN = "http://localhost:5000/"
fileName = "service1_results.txt"

def timeStamp():
    return round(time.time() * 1000)

with open(fileName, "w") as f:

    for i in range(5):
        timeStart = timeStamp()
        response = requests.get(SERVICE1_DOMAIN + "Quotes/random")
        timeEnd = timeStamp()

        if response.status_code == 200:
            f.write("TEST " + str(i+1) + " --> STATUS CODE 200 - RESPONSE OK\nresponse time = " + str(timeEnd - timeStart) + "\n\n")
        else:
            f.write("TEST " + str(i+1) + " --> STATUS CODE " + str(response.status_code) + " - REQUEST FAILED\nresponse time = " + str(timeEnd - timeStart) + "\n\n")
            
    f.close()

print("Printed results to file '" + str(fileName) + "'")