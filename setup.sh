#!/bin/bash

ROOT_FOLDER=$(pwd)

# Set up service 1

# Create virtual environment
cd service1
python3 -m venv venv
. venv/bin/activate

cd service

# Install dependencies
pip3 install -r requirements.txt

# Set FLASK_APP
export FLASK_APP app

# Initialise and populate database
python3 -m flask db init
python3 -m flask db migrate
python3 -m flask db upgrade

python3 db_populate.py

# Clean up
deactivate
cd $ROOT_FOLDER


# Set up service 2

# Create virtual environment
cd service2
python3 -m venv venv
. venv/bin/activate

cd service

# Install dependencies
pip3 install -r requirements.txt

# Set FLASK_APP
export FLASK_APP app

# Clean up
deactivate
cd $ROOT_FOLDER


# Set up client

# Create virtual environment
cd client
python3 -m venv venv
. venv/bin/activate

cd clientService

# Install dependencies
pip3 install -r requirements.txt

# Set FLASK_APP
export FLASK_APP app

# Clean up
deactivate
cd $ROOT_FOLDER


# Set up tests

# Create virtual environment
cd "test"
python3 -m venv venv
. venv/bin/activate

# Install dependencies
pip3 install -r requirements.txt

# Clean up
deactivate
cd $ROOT_FOLDER